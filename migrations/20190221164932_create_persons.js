
exports.up = function(knex, Promise) {
  return knex.schema.createTable('persons',function(table){
    table.increments('id');
    table.string('name');
    table.string('age');
    table.string('gender');
    table.integer('role_id').unsigned();
    table.foreign('role_id').references('id').on('roles');
    table.timestamps();
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('persons');
};
