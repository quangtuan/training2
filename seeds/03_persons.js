
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').del()
    .then(function () {
      // Inserts seed entries
      return knex('persons').insert([
        {id: 1, name: 'Quang Tuan', age: '22' , gender: 'Male', role_id: 1},
        {id: 2,  name: 'Van Khuong', age: '22' , gender: 'Male', role_id: 2},
        {id: 3,  name: 'Thuy Ngoc', age: '20' , gender: 'Female', role_id: 1}
      ]);
    });
};
