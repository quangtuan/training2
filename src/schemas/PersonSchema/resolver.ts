import { PersonModel } from '../../models/persons';


export const resolver = {
    Query: {
        getAllPerson: () => PersonModel.getAllPerson(),
        findPerson: (_,{id}  ) => PersonModel.findPerson(id)

    },

    Mutation: {
        createPerson(root, {input}){
            return PersonModel.createPerson(input);
        },

        updatePerson(root, {id, input}){
            if(id){
                return PersonModel.updatePerson(id, input);
            }
            return false;
        }

    }
}
